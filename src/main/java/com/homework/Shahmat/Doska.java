package com.homework.Shahmat;

/**
 * Created by PC_11 on 29.01.2015.
 */
public class Doska {

    private Cell[][] field=new Cell[8][8];

    private static class Cell{
        Figure figure=null;

    }


    public boolean putFigure(Figure figure, int row,int col) {
        if (field[row][col].figure !=null ) return false;

        field[row][col].figure=figure;
        return true;
    }
}
